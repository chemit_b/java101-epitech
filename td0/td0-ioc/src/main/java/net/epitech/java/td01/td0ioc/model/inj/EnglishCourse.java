package net.epitech.java.td01.td0ioc.model.inj;

import javax.inject.Inject;
import javax.inject.Named;

import net.epitech.java.td01.td0ioc.model.AbstractTeachable;
import net.epitech.java.td01.td0ioc.model.contract.Nameable;
import net.epitech.java.td01.td0ioc.model.contract.Teachable;
import net.epitech.java.td01.td0ioc.model.types.CourseType;

public class EnglishCourse extends AbstractTeachable /* optional */implements
		Teachable {

	@Inject
	@Named("english")
	private Nameable teacher;

	public CourseType getCourse() {
		return CourseType.ENGLISH;
	}

	public Nameable getTeacher() {
		return teacher;
	}

	public String getCourseMaterial() {
		return "I love shakespeare";
	}

}
