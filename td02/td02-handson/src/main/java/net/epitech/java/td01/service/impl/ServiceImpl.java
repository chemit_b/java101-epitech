package net.epitech.java.td01.service.impl;

import java.util.Collection;
import java.util.HashSet;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;
import net.epitech.java.td01.service.contract.Service;

//TODO:this class should be unit tested
@org.springframework.stereotype.Service
public class ServiceImpl implements Service
{
	public ServiceImpl()
	{
		this.courses = new HashSet<Course>();
		this.teachers = new HashSet<Teacher>();
		try {
			this.addTeacher("miramond", "miramo_m@epitech.eu", 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Collection<Course> getCourses()
	{
		return this.courses;
	}

	public Collection<Teacher> getTeachers()
	{
		return this.teachers;
	}

	public void addCourse(Course c)
	{
		this.courses.add(c);
	}

	public void deleteCourse(Course c)
	{
		this.courses.remove(c);
	}

	public void addTeacher(String name, String mail, Integer id) throws Exception
	{
		boolean exist = false;

		for (Teacher t : this.teachers)
		{
			if (t.getId().equals(id))
				exist = true;
		}
		if (!exist)
		{
			Teacher t = new Teacher();
			t.setId(id);
			t.setName(name);
			t.setMail(mail);
			this.teachers.add(t);			
		}
		else
			throw new Exception("Teacher already exist");
	}

	public void deleteTeacher(Integer id) throws Exception
	{
		boolean exist = false;
		Collection<Teacher> teacherstodelete = new HashSet<Teacher>();

		for (Teacher t : this.teachers)
		{
			if (t.getId().equals(id))
			{
				teacherstodelete.add(t);
				exist = true;
			}
		}
		this.teachers.removeAll(teacherstodelete);
		if (!exist)
			throw new Exception("Teacher does not exist");			
	}

	public Collection<Pair<DateTime, DateTime>> getAvaibleTimeSlot(Duration d)
	{
		Collection<Pair<DateTime, DateTime>> avaibleTimeSlot = new HashSet<Pair<DateTime, DateTime>>();
		return avaibleTimeSlot;
	}
	
	private Collection<Course>	courses;
	private Collection<Teacher>	teachers;

}
