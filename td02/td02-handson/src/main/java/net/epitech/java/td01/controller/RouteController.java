package net.epitech.java.td01.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;
import net.epitech.java.td01.service.contract.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

//je suis une class de type controller dans spring = je peux être injectée et je fais office de servlet.
@Controller
public class RouteController {

	@Autowired
private Service service;
//	
////	//TODO: use field injection
////	@Autowired
////	public RouteController(Service service) {
////		this.service = service;
////	}
//	
//	// je récupère tout ce qui vient sur l'url / à la racine de l'application
//	@RequestMapping(value = "/")
//	public ModelAndView test(HttpServletResponse response) throws IOException {
//		return new ModelAndView("home");
//	}
//	
//	@RequestMapping(value = "/profs")
//	public ModelAndView getTeachers(HttpServletResponse response) throws IOException {
//	    final Map<String, Collection<Teacher>> paramsMap = new HashMap();
//
//	    paramsMap.put("teachers", this.service.getTeachers());
//		return new ModelAndView("teachers", paramsMap);
//	}
//	
//	//TODO: duplicate Request Mapping Definition
//	@RequestMapping(value = "/cours")
//	public ModelAndView getCourses(HttpServletResponse response) throws IOException {
//		final Map<String, Collection<Course>> paramsMap = new HashMap();
//		
//		paramsMap.put("courses", this.service.getCourses());	    
//		return new ModelAndView("courses", paramsMap);
//	}
//	
	@RequestMapping(value = "/nouveau-cours")
	public ModelAndView addNewCourses(HttpServletResponse response, @ModelAttribute("course") Course course) throws IOException {
		System.out.println("Activity:" + course.getActivity());

		return new ModelAndView("home");
	}
	
	@RequestMapping(value = "/test")
	public ModelAndView newCourse(HttpServletResponse response) throws IOException {
	
		return new ModelAndView("test","command",new Course());
	}
}

