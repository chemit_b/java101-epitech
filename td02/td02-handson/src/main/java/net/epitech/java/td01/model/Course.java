package net.epitech.java.td01.model;

import org.joda.time.DateTime;
import org.joda.time.Duration;

public class Course
{
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public DateTime getDate() {
		return date;
	}
	public void setDate(DateTime date) {
		this.date = date;
	}
	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	private String		module;
	private String		activity;
	private DateTime	date;
	private Duration	duration;
	private Teacher		teacher;
	private State		state;
}
